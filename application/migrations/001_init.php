<?php

class Migration_Init extends CI_Migration {

    public function up() {
        $this->db->trans_start();

        //For individual artworks
        $this->db->query('
            CREATE TABLE IF NOT EXISTS work (
            id INT NOT NULL AUTO_INCREMENT,
            title VARCHAR(100) NOT NULL,
            category VARCHAR(100) NOT NULL,
            year YEAR(4) NOT NULL,
            description TEXT NOT NULL,
            size VARCHAR(25),
            materials TEXT,
            published TINYINT NOT NULL DEFAULT 0,
            PRIMARY KEY (id)
            )');

        // For media related to specific artworks (uploaded pictures/vimeo links etc)
        $this->db->query('
            CREATE TABLE IF NOT EXISTS work_media (
            id INT NOT NULL AUTO_INCREMENT,
            related_id INT NOT NULL,
            type VARCHAR(25),
            url VARCHAR(100),
            file VARCHAR(50),
            PRIMARY KEY (id),
            FOREIGN KEY (related_id) REFERENCES work(id)
            )');

        //Table for related (links/works/exh/texts)
        $this->db->query('
            CREATE TABLE IF NOT EXISTS work_related (
            id INT NOT NULL AUTO_INCREMENT,
            related_id INT NOT NULL,
            title VARCHAR(75),
            type VARCHAR(25),
            url VARCHAR(100),
            file VARCHAR(50),
            PRIMARY KEY (id),
            FOREIGN KEY (related_id) REFERENCES work(id)
            )');

        // Exhibitions
        $this->db->query('
            CREATE TABLE IF NOT EXISTS exhibition (
            id INT NOT NULL AUTO_INCREMENT,
            title VARCHAR(75),
            date VARCHAR(25),
            location VARCHAR(100),
            city VARCHAR(50),
            published TINYINT NOT NULL DEFAULT 0,
            PRIMARY KEY (id)
            )');

        //Exhibitions media
        $this->db->query('
            CREATE TABLE IF NOT EXISTS exhibition_media (
            id INT NOT NULL AUTO_INCREMENT,
            related_id INT NOT NULL,
            type VARCHAR(25),
            url VARCHAR(100),
            file VARCHAR(50),
            PRIMARY KEY (id),
            FOREIGN KEY (related_id) REFERENCES exhibition(id)
            )');

        //
        //Table for related (links/works/exh/texts)
        $this->db->query('
            CREATE TABLE IF NOT EXISTS exhibition_related (
            id INT NOT NULL AUTO_INCREMENT,
            related_id INT NOT NULL,
            title VARCHAR(75),
            type VARCHAR(25),
            url VARCHAR(100),
            file VARCHAR(50),
            PRIMARY KEY (id),
            FOREIGN KEY (related_id) REFERENCES exhibition(id)
            )');

        $this->db->query('
            CREATE TABLE IF NOT EXISTS text (
            id INT NOT NULL AUTO_INCREMENT,
            title VARCHAR(75),
            author VARCHAR(100),
            date YEAR(4),
            published TINYINT NOT NULL DEFAULT 0,
            PRIMARY KEY (id)
            )');

        $this->db->query('
            CREATE TABLE IF NOT EXISTS text_media (
            id INT NOT NULL AUTO_INCREMENT,
            related_id INT NOT NULL,
            type VARCHAR(25),
            url VARCHAR(100),
            file VARCHAR(50),
            PRIMARY KEY (id),
            FOREIGN KEY (related_id) REFERENCES text(id)
            )');
        $this->db->trans_complete();

        //sessions
        $fields = array(
            'session_id VARCHAR(40) DEFAULT \'0\' NOT NULL',
            'ip_address VARCHAR(45) DEFAULT \'0\' NOT NULL',
            'user_agent VARCHAR(120) NOT NULL',
            'last_activity INT(10) unsigned DEFAULT 0 NOT NULL',
            'user_data text NOT NULL'
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('session_id', TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query('ALTER TABLE `ci_sessions` ADD KEY `last_activity_idx` (`last_activity`)');

        //user

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '128',
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('users');
    }

    public function down(){
        $this->db->trans_start();
        $this->db->query('SET foreign_key_checks = 0');
        $this->db->query('DROP TABLE IF EXISTS work');
        $this->db->query('DROP TABLE IF EXISTS work_media');
        $this->db->trans_complete();
    }

}