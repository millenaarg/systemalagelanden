<html>
<head>
    <title>Upload Form</title>
</head>
<body>

<h3>Upload successful</h3>

<p>The uploaded Image:</p>

<img alt="uploaded image" src="<?=base_url(). 'media/' . $upload_data['raw_name'].$upload_data['file_ext'];?>">

<p>The Thumbnail Image:</p>

<img alt="Thumbnail image" src="<?=base_url(). 'media/' . $upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];?>">
<p><?php echo anchor('admin/dashboard', 'Return to dashboard'); ?></p>

</body>
</html>