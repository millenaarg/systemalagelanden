<body>
<!--================ Navigation ==============
<div class="top-sub-nav-fixed no1">
    <ul class="nav sub-nav">
        <li><a href="<?php echo site_url('admin/dashboard/index/training');?>">Onze Training</a></li>
        <li><a href="<?php echo site_url('admin/dashboard/index/plaats-tijd-kosten');?>">Plaats, tijden, kosten</a></li>
        <li><a href="<?php echo site_url('admin/dashboard/index/trainers');?>">Trainers</a></li>
        <li><a href="<?php echo site_url('admin/dashboard/index/seminars');?>">Seminars</a></li>
    </ul>
</div>

<div class="top-sub-nav-fixed no2">
    <ul class="nav sub-nav">
        <li><a href="<?php echo site_url('admin/dashboard/index/introductie');?>">Introductie</a></li>
        <li><a href="<?php echo site_url('admin/dashboard/index/videos');?>">Videos</a></li>
        <li><a href="<?php echo site_url('admin/dashboard/index/meer-info');?>">Meer info</a></li>
    </ul>
</div>
<div class="top-sub-nav-fixed no3">
    <ul class="nav sub-nav">

        <li><a href="<?php echo site_url('admin/dashboard/manage_uploads');?>">Manage uploads</a></li>
        <li><a href="<?php echo site_url('admin/user/logout');?>">Logout</a></li>
    </ul>
</div>

<div class="top-nav-fixed">
    <div class="top-logo"><a href="<?php echo site_url('admin/dashboard/index/home');?>">Система Lage Landen</a></div>
    <ul class="nav">
        <li class="info-menu no1">Trainen</li>
        <li class="info-menu no2">Over Systema</li>
        <li><a href="<?php echo site_url('admin/dashboard/index/english');?>">English</a></li>
        <li><a href="<?php echo site_url('admin/dashboard/index/contact');?>">Contact</a></li>
        <li class="info-menu no3">Aдмин</li>
    </ul>
</div>
-->

<?php


foreach ($this->data['menu_content'] as $items){
    echo "\n\n<div class=\"top-sub-nav-fixed no{$items[0]['parent']}\">\n";
    echo "<ul class=\"nav sub-nav\"> \n";
    foreach ($items as $item){
        if($item['is_admin'] == 0) {
            echo "<li><a href=\"" . site_url("admin/dashboard/index/{$item['content']}") . "\">" . $item['title'] . "</a></li> \n";
        }else{
            echo "<li><a href=\"" . site_url("admin/{$item['content']}") . "\">" . $item['title'] . "</a></li> \n";
        }
    }
    echo "</ul></div>\n\n";
}
?>


<div class="top-nav-fixed">
    <div class="top-logo"><a href="<?php echo site_url('admin/dashboard/index/home');?>">Система Lage Landen</a></div>
    <ul class="nav"> <?php echo "\n";
        foreach ($this->data['top_menu'] as $top_item){
            if ($top_item['parent'] == 0 && $top_item['type'] == 'parent'){
                echo "<li class=\"info-menu no{$top_item['id']}\">" . $top_item['title'] . "</li> \n";
            } else {
                echo "<li><a href=\"" . site_url("admin/dashboard/index/{$top_item['content']}") . "\">" . $top_item['title'] ."</a></li> \n";
            }
        }
        ?>
    </ul>
</div>