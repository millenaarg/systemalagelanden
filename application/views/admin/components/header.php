<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo base_url('css/normalize.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/style.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/redactor.css');?>">
    <script src="<?php echo base_url('js/jquery-2.1.3.min.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/redactor.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/table/table.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/video/video.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/fullscreen/fullscreen.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/imagemanager/imagemanager.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/filemanager/filemanager.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/fontsize/fontsize.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/fontfamily/fontfamily.js');?>"></script>
    <script src="<?php echo base_url('js/redactor/plugins/fontcolor/fontcolor.js');?>"></script>

    <title>Systema Lage Landen</title>
    <script>
        $(document).ready(function (){
            $('.info-menu').click(function(){
                // Works on the assumption that clicked classes share an identical class with "submenu" classes.
                // And that this is the second element from the classList array.
                // Append proper class to generic submenu name.
                var menuNumber = this.classList[1];
                var menuToggle = '.top-sub-nav-fixed.' + menuNumber;
                // If other sub menu item is active, toggle active
                $('.top-sub-nav-fixed').each(function (){
                    if (typeof this.classList[2] != 'undefined' && this.classList[1] != menuNumber ) {
                        $(this).removeClass('active');
                    }
                });
                // Toggle appropriate menu item
                $(menuToggle).toggleClass('active');
            });

            $(function(){
                $('#content').redactor({
                    toolbarFixedTopOffset: 100,
                    toolbarFixed: true,
                    imageUpload: '<?php echo base_url('index.php/admin/upload/do_upload');?>',
                    imageManagerJson: '<?php echo base_url('index.php/admin/images?format=json');?>',
                    fileManagerJson: '<?php echo base_url('index.php/admin/files?format=json');?>',
                    fileUpload: '<?php echo base_url('index.php/admin/upload/do_upload');?>',
                    plugins: ['table','video','fullscreen','imagemanager','filemanager','fontsize','fontfamily','fontcolor']
                });
            });
        });
    </script>
</head>