<div class="no-script"><noscript>Please enable JavaScript!</noscript></div>
<script>
    function confirmDelete(id, name, url) {
       var r =  confirm(name + " verwijderen?");
        if (r == true) {
            $.ajax(url + "/" + id);
            setTimeout( function(){location.reload(true);}, 200) ;
        }
    }
</script>
<div class="main-container">

    <div class="content-container">
        <section>
        <h2 id="ploaties">Ploaties <span class="file-link"><a href="#bestaandn">Bestaandn</a></span></h2>

            <?php if($images->num_rows != 0): foreach($images->result() as $image): ?>
                <div class="thumb-container">
                    <div class="thumb">
                        <?php echo '<img class="thumb" src="'. base_url('img/'. $image->thumb ). '"/>'; ?>
                    </div>
                    <div class="name">
                        <?php echo $image->name; ?>
                    </div>
                    <div class="timestamp">
                        <?php echo $image->timestamp;?>
                    </div>
                    <div class="delete-upload">
                        <button <?php echo 'onClick = confirmDelete(' . $image->id . ',"'. $image->name .'","'. base_url("index.php/admin/dashboard/remove/") .'")';?>>X</button>
                    </div>
                </div>
            <?php endforeach ;?>
            <?php else: ?>
                Geen Ploaties!
            <?php endif ;?>
        </section>

        <h2 id="bestaandn">Bestaandn <span class="file-link"><a href="#ploaties">Ploaties</a></span></h2>
        <table class="images-table">
            <thead>
            <tr>
                <th>Naam</th>
                <th>Upload datum/tijd</th>
                <th>Verwijder</th>
            </tr>
            </thead>
            <tbody>
            <?php if($files->num_rows != 0): foreach($files->result() as $file): ?>

                <tr>
                    <td> <?php echo $file->name; ?> </td>
                    <td> <?php echo $file->timestamp; ?> </td>
                    <td><button <?php echo 'onClick = confirmDelete(' . $file->id . ',"'. $file->name .'","'. base_url("index.php/admin/dashboard/remove/") .'")';?>>X</button></td>
                </tr>
            <?php endforeach ;?>
            <?php else: ?>
                <tr>
                    <td colspan="4">Geen Bestaandn!</td>
                </tr>
            <?php endif ;?>
            </tbody>
        </table>
        </section>

        </div>