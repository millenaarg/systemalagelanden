<section>
    <h2>Users</h2>
    <?php echo anchor('admin/user/edit', 'Add User');?>
    <table class="work-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
<?php if(count($users)): foreach($users as $user): ?>

    <tr>
        <td> <?php echo anchor('admin/user/edit/' . $user->id, $user->name); ?> </td>
        <td> <?php echo btn_edit('admin/user/edit/' . $user->id); ?> </td>
        <td> <?php echo btn_delete('admin/user/delete/' . $user->id); ?> </td>
    </tr>
<?php endforeach ;?>
<?php else: ?>
        <tr>
            <td colspan="3">No work s found</td>
        </tr>
<?php endif ;?>
        </tbody>
    </table>
</section>