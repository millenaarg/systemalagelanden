<div class="login-container">
    <?php echo validation_errors();?>
    <?php echo form_open();?>
    <table class="login-table">
        <tr>
            <td>Email</td>
            <td><?php echo form_input('email');?></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><?php echo form_password('password');?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_submit('submit', 'Log in', 'class="login-button"');?></td>
        </tr>
    </table>
    <?php echo form_close();?>


</div>