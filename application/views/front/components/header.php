<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo base_url('css/normalize.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/style.css');?>">
    <script src="<?php echo base_url('js/jquery-2.1.4.min.js');?>"></script>

    <title>Systema Lage Landen</title>
    <script>
        $(document).ready(function (){
            $('.info-menu').click(function(){
                // Append proper class to generic submenu name.
                var menuNumber = this.classList[1];
                var menuToggle = '.top-sub-nav-fixed.' + menuNumber;
                // If other sub menu item is active, toggle active
                $('.top-sub-nav-fixed').each(function (){
                   if (typeof this.classList[2] != 'undefined' && this.classList[1] != menuNumber ) {
                       $(this).removeClass('active');
                   }
                });
                // Toggle appropriate menu item
                $(menuToggle).toggleClass('active');
            });
        });
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
        ga('create', 'UA-61447778-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>