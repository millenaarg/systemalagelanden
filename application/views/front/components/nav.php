<body>
<!--================ Navigation ==============-->


<?php
foreach ($this->data['menu_content'] as $items){
    echo "\n\n<div class=\"top-sub-nav-fixed no{$items[0]['parent']}\">\n";
    echo "<ul class=\"nav sub-nav\"> \n";
    foreach ($items as $item){
        echo "<li><a href=\"" . site_url("front/welcome/index/{$item['content']}") . "\">" . $item['title'] . "</a></li> \n";
    }
    echo "</ul></div>\n\n";
}
?>


<div class="top-nav-fixed">
    <div class="top-logo"><a href="<?php echo base_url();?>">Система Lage Landen</a></div>
    <ul class="nav"> <?php echo "\n";
                    foreach ($this->data['top_menu'] as $top_item){
                        if ($top_item['parent'] == 0 && $top_item['type'] == 'parent'){
                            echo "<li class=\"info-menu no{$top_item['id']}\">" . $top_item['title'] . "</li> \n";
                        } else {
                            echo "<li><a href=\"" . site_url("front/welcome/index/{$top_item['content']}") . "\">" . $top_item['title'] ."</a></li> \n";
                        }
                    }
                    ?>
    </ul>
</div>

