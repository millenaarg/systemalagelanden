<?php
class MY_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
        // enable name/value pairs to be passed by url. Method name is first name!
        $this->params = $this->uri->uri_to_assoc(4);
        $this->output->enable_profiler(false);
    }
}