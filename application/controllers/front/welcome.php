<?php
class Welcome extends Frontend_Controller {
    public function __construct () {
        parent::__construct();
        $this->load->model('welcome_m');
        $this->load->helper('url');

    }

    public function index () {
        // check for uri segments
        if($this->uri->segment(4)){
            $this->data['page'] = $this->uri->segment(4);
        }
        else {
            $this->data['page'] = "home";
        };

        // get page associated to uri segment, input is sanitized by active record class
        $this->data['content'] = $this->welcome_m->get_content($this->data['page']);

        // if empty array is returned, get data for homepage
        if(empty($this->data['content'])){
            $this->data['content'] = $this->welcome_m->get_content('home');
        };

        $this->data['menu_content'] = $this->welcome_m->get_menus(); // Get data to build menus in view
        $this->data['top_menu'] = array_shift($this->data['menu_content']); // separate top menu and sub menus in different arrays
        
        $this->load->view('front/welcome/index', $this->data);
    }



}