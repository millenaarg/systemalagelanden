<?php

class Files extends REST_Controller
{


    public function index_get()
    {
        if ($this->session->userdata('logged_in')) {
            $this->db->from('uploads');
            $this->db->where('type', 'file');
            $this->db->select('name');
            $this->db->select('link');
            $this->db->select('title');
            $this->db->select('size');
            $results = $this->db->get()->result_array();
            // Retrieve last key of the associative array.
            end($results);
            $counter = key($results);
            reset($results);
            // Prepend the image directory to the filenames.
            if (isset($counter)) {
                for ($X = 0; $X <= $counter; $X++) {
                    $results[$X]['link'] = base_url('img') . '/' . $results[$X]['link'];
                }
            }

            $this->response($results);

        }
        else
        {
            $this->response('not allowed');
        }
    }

}