<?php
class Dashboard extends Admin_Controller {
    public function __construct () {
        parent::__construct();
        $this->load->model('admin_m');
        $this->load->helper('url');
        $this->data['menu_content'] = $this->admin_m->get_menus(); // Get data to build menus in view
        $this->data['top_menu'] = array_shift($this->data['menu_content']); // separate top menu and sub menus in different arrays



    }

    public function index () {

        if($this->uri->segment(4)){
            $this->data['page'] = $this->uri->segment(4);
        }
        else {
            $this->data['page'] = "home";
        }
        if(isset($_POST['content'])){

            $this->data['posted'] = $this->admin_m->update_page($_POST['content'],$this->data['page']);
        }
        $this->data['content'] = $this->admin_m->get_content($this->data['page']);



        $this->load->view('admin/dashboard/index', $this->data);
    }

    public function admin () {
        $this->load->view('admin/dashboard/manage_uploads', $this->data);
    }

    public function manage_uploads (){
        $this->data['images'] = $this->admin_m->get_uploads('images');
        $this->data['files'] = $this->admin_m->get_uploads('files');
        $this->load->view('admin/dashboard/uploads', $this->data);
    }
    public function remove ($id) {
        $this->admin_m->remove_upload($id);
    }


}