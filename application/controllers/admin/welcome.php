<?php
class Welcome extends Admin_Controller {
    public function __construct () {
        parent::__construct();
        $this->load->model('admin_m');
        $this->load->helper('url');

    }

    public function index () {

        if($this->uri->segment(4)){
            $this->data['page'] = $this->uri->segment(4);
        }
        else {
            $this->data['page'] = "home";
        }
        if(isset($_POST['content'])){

            $this->data['posted'] = $this->admin_m->update_page($_POST['content'],$this->data['page']);
        }
        $this->data['content'] = $this->admin_m->get_content($this->data['page']);
        $this->load->view('admin/welcome/index', $this->data);
    }



}