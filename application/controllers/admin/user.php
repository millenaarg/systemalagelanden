<?php
class User extends Admin_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index () {

        // Fetch all users
        $this->data['users'] = $this->user_m->get_all();

        //Load view
        $this->data['subview'] = 'admin/user/index';
        $this->load->view('admin/_admin_menu', $this->data);
    }

    public function edit ($id = null) {
        // Fetch user or set new one
        if ($id) {
            $this->data['user'] = $this->user_m->get($id);
            count($this->data['user']) || ($this->data['errors'][] = 'User could not be found');
        }
        else {
            $this->data['user'] = $this->user_m->get_new();
        }

        //set up the form
        $rules = $this->user_m->rules_admin;
        $id || $rules['password']['rules'] .= '|required';
        $this->form_validation->set_rules($rules);

        // process form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->user_m->array_from_post(array('name', 'email', 'password'));
            if(!empty($data['password'])) {
                $data['password'] = $this->user_m->hash($data['password']);
            } else {
                // don't save an empty password
                unset($data['password']);
            }
            $this->user_m->update($id, $data);
            redirect('admin/user');
        }
        // Load the view
        $this->data['subview'] = 'admin/user/edit';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function delete ($id) {
        $this->user_m->delete($id);
        redirect('admin/user');

    }

    public function login () {

        //Redirect if logged in
        $dashboard = 'admin/dashboard/manage_uploads';
        $this->user_m->logged_in() == false || redirect($dashboard);

        // Set up form
        $rules = $this->user_m->rules;
        $this->form_validation->set_rules($rules);

        // Process form
        if ($this->form_validation->run() == true){
            //login and redirect
            if ($this->user_m->login() == true) {
                redirect($dashboard);
            }
            else {
                $this->session->set_flashdata('error', 'Login unknown');
                redirect('admin/user/login', 'refresh');
            }
        }
        // Load view
        $this->data['subview'] = 'admin/user/login';
        $this->load->view('admin/_layout_modal', $this->data);
    }

    public function logout (){
        $this->user_m->logout();
        redirect('admin/user/login');
    }


}