<?php
class Upload extends Admin_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->model('upload_model');
        $this->load->library('upload');
    }

    function do_upload()
    {
        // files storage folder
        $dir = getcwd(). '/' . 'img/' ;//'D:\\Bitnami\\wampstack-5.4.31-0\\apache2\\htdocs\\systemala\\master2\\public_html\\img\\' ;// getcwd(). '/' . 'img/' ;

        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);

        if ($_FILES['file']['type'] == 'image/png'
            || $_FILES['file']['type'] == 'image/jpg'
            || $_FILES['file']['type'] == 'image/gif'
            || $_FILES['file']['type'] == 'image/jpeg'
            || $_FILES['file']['type'] == 'image/pjpeg')
        {$type = 'image';}
        else {$type = 'file';}

            $file = $dir .$_FILES['file']['name'];
            $pathinfo = pathinfo($file);

            // Check if exists
            if (get_file_info($file) == false) {
                move_uploaded_file($_FILES['file']['tmp_name'], $file);
            } else {
                // IF filename exists append _DN and recurse
                $_FILES['file']['name'] = $pathinfo['filename'] . '_DN'. '.' . $pathinfo['extension'];
                $this->do_upload($_FILES);
                exit;
            }

            //Create thumbnail
            if ($type == 'image') {
                $this->create_thumb($file);
                // REGISTER FILE IN DB
                $image_data = array('name' => $pathinfo['basename'], 'image' => $pathinfo['basename'], 'type' => $type, 'thumb' => $pathinfo['filename'] . '_thumb.' . $pathinfo['extension'], 'size' => $_FILES['file']['size']);
                $this->upload_model->add_file($image_data);
                // displaying file
                $array = array(
                    'filelink' => base_url('img') . '/' . $pathinfo['basename']
                );
            }
            if ($type == 'file'){
                $file_data = array('name' => $pathinfo['basename'], 'type' => $type , 'link' => $pathinfo['basename'], 'size' => $_FILES['file']['size'], 'title' => $pathinfo['filename']);
                $this->upload_model->add_file($file_data);
                // displaying file
                $array = array(
                    'filelink' => base_url('img') . '/' . $pathinfo['basename'],
                    'filename' => $pathinfo['basename']
                );
            }
            echo stripslashes(json_encode($array));

        }

    function create_thumb($file)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $file;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['height'] = 175;
        $config['width'] = 175;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }
}