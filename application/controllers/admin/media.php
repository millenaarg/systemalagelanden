<?php
class Media extends Admin_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('media_m');

    }

    public function add_media () {
        $this->data['parent_id'] = $this->uri->segment(4);
        $this->data['parent_table'] = $this->uri->segment(5);
        $this->data['type'] = $this->uri->segment(6);

        $this->data['subview'] = 'admin/media/add_' . $this->uri->segment(6);
        $this->load->view('admin/_admin_menu', $this->data);
    }

    public function do_upload () {

        // Set variables in case of failed upload
        if ( $this->input->post('type') == TRUE )
        {
            $type = $this->input->post('type');
            $parent_table = $this->input->post('parent_table');
            $parent_id = $this->input->post('parent_id');
        } else {
            $type = $this->uri->segment(6);
            $parent_table =  $this->uri->segment(5);
            $parent_id =  $this->uri->segment(4);
        }

        // pass proper variables in case of failure
        $this->data['type'] = $type;
        $this->data['parent_table'] = $parent_table;
        $this->data['parent_id'] = $parent_id;


        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['max_size']	= '2048';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $this->data['error'] = array('error' => $this->upload->display_errors());
            $subview = 'admin/media/add_' . $type;
            $this->data['subview'] = $subview;
            $this->load->view('admin/_admin_menu', $this->data);
        }
        else
        {
            $data  = $this->upload->data();
            $this->thumb($data);
            $post = $this->input->post();


            //register image
            $this->media_m->register_image($post, $data);
            redirect('admin/'. $this->input->post('parent_table') .'/edit/'. $this->input->post('parent_id'));

        }
    }

    function thumb($data)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'media/' . $data['raw_name'] . $data['file_ext'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 275;
        $config['height'] = 250;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }


    public function edit ($id) {


        $medium = $this->media_m->get($id);
        switch ($medium->type){
            case 'img':
                $this->data['subview'] = 'admin/media/edit_img';
                $this->load->view('admin/_admin_menu', $medium);
                break;
            case 'pdf':
                $this->data['subview'] = 'admin/media/edit_pdf';
                $this->load->view('admin/_admin_menu', $medium);
                break;
            case 'vim':
                $this->data['subview'] = 'admin/media/edit_vim';
                $this->load->view('admin/_admin_menu', $medium);
                break;

        }
    }

    public function edit_img (){

        $this->media_m->update($this->input->post('id'), array('title'=> $this->input->post('title') , 'ordering' => $this->input->post('order'), 'description'=> $this->input->post('description')));
        redirect('admin/'. $this->input->post('parent_table') .'/edit/'. $this->input->post('parent_id'));
    }

    public function set_thumb ($id){
        $data = $this->media_m->get($id);
        var_dump($data);

        $parent_table = $data->parent_table;
        $parent_id = $data->parent_id;

        $uri = $data->uri;
        $thumb = $this->get_thumb_uri($uri);


        $this->db->where('id', $parent_id);
        $this->db->update($parent_table , array ('thumb' => $thumb));
        redirect('admin/work');
    }
    public function set_pdf ($id){
        $data = $this->media_m->get($id);

        $parent_table = $data->parent_table;
        $parent_id = $data->parent_id;

        $uri = $data->uri;


        $this->db->where('id', $parent_id);
        $this->db->update($parent_table , array ('pdf' => $uri));
        redirect('admin/text');
    }


    public function get_thumb_uri($uri){
        $ext = substr($uri, -4);
        $front = substr($uri, 0, -4);
        $thumb = $front . '_thumb' . $ext;
        return $thumb;
    }

    public function delete ($id) {

        $redirect = $this->get_redirect($id);
        $this->media_m->delete_from_disk($id);
        redirect($redirect);
    }

    public function get_redirect($id){
        $this->db->select('parent_table, parent_id');
        $this->db->from('media');
        $this->db->where('id', $id);
        $result = $this->db->get();
        $row = $result->row();

        return "admin/" . $row->parent_table . "/edit/" . $row->parent_id;
    }

    public function edit_vimeo ( $id = null ) {

            // Fetch work or set new one
            if ($id) {
                $this->data['vimeo'] = $this->media_m->get($id);
                count($this->data['vimeo']) || ($this->data['errors'][] = 'Media could not be found');
            }
            else {
                $this->data['vimeo'] = $this->media_m->get_new();
            }

            //set up the form
            $rules = $this->media_m->validate;
            $this->form_validation->set_rules($rules);

            // process form

            if ($this->form_validation->run() == TRUE && $id == null) {
                $data = $this->media_m->array_from_post(array('parent_table', 'parent_id', 'uri', 'title', 'type', 'published'));
                $data['ordering'] = $this->media_m->get_new_order($data);
                $this->media_m->insert($data);
                $redirect = 'admin/' . $this->input->post('parent_table') . "/edit/" . $this->input->post('parent_id');
                redirect($redirect);
            }
            if ($this->form_validation->run() == TRUE && $id !== null) {
                $data = $this->media_m->array_from_post(array('parent_table', 'parent_id', 'uri', 'title', 'type', 'published', 'ordering'));
                $this->media_m->update($id, $data);
                $redirect = 'admin/' . $this->input->post('parent_table') . "/edit/" . $this->input->post('parent_id');
                redirect($redirect);
            }

//            // Set checkbox
//            if ($this->data['work']->published != 0 ){
//
//                $this->data['published_button'] = TRUE;
//
//            } else {
//                $this->data['published_button'] = FALSE;
//            }

            //Load the view
            $this->data['subview'] = 'admin/media/add_vim';
            $this->load->view('admin/_admin_menu', $this->data);
        }




}