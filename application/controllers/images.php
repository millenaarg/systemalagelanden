<?php

class Images extends REST_Controller
{
    public function index_get()
    {
        $this->db->from('uploads');
        $this->db->where('type', 'image');
        $this->db->select('thumb');
        $this->db->select('image');
        $results = $this->db->get()->result_array();
        end($results);
        $counter = key($results);
        reset($results);
        if (isset($counter)) {
            for ($X = 0; $X <= $counter; $X++) {
                $results[$X]['thumb'] = base_url('img') . '/' . $results[$X]['thumb'];
                $results[$X]['image'] = base_url('img') . '/' . $results[$X]['image'];
            }
        }

        $this->response($results);

    }
}