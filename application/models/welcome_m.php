<?php
class Welcome_m extends MY_Model {


    public function get_content($slug){
        $this->db->from('pages');
        $this->db->where('slug', $slug);
        $content = $this->db->get();
        return $content->result_array();
    }

    public function get_menus(){
        // Get normalized menu array
        // $menu[menu number][item info] -> Top menu has number 0, sub menus have subsequent numbers, menu[1],menu[2] etc
        $this->db->from('menu');
        $this->db->where('is_admin', 0);
        $menu_content = $this->db->get();
        $results = $menu_content->result_array();
        foreach($results as $result){
            if ($result['type'] == 'parent' || ($result['type'] == 'link' && $result['parent'] == 0)){
                $menu[0][] = $result; // Topmenu items go in menu[0], subsequent sub menus in menu[1],menu[2] etc
            } else {
                $menu[$result['parent']][] = $result;
            }
        }
        return $menu;
    }

}