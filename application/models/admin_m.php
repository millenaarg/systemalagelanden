<?php
class Admin_m extends MY_Model {


    public function get_content($slug){
        $this->db->from('pages');
        $this->db->where('slug', $slug);
        $content = $this->db->get();
        return $content->result_array();
    }

    public function update_page($content, $slug){
        $this->db->where('slug', $slug);
        $this->db->update('pages',array('content' => $content));
        return true;
    }

    public function get_uploads ($type) {
        if ($type == 'images'){
            $query = $this->db->get_where('uploads', array('type'=>'image'));
            return $query;
        }
        if ($type == 'files'){
            $query = $this->db->get_where('uploads', array('type'=>'file'));
            return $query;
        }
    }

    public function remove_upload ($id) {
        // check if file or image
        // if file, remove file and db entry
        //if image, remove file, thumb and db entry
        $upload = $this->db->get_where('uploads', array('id'=> $id))->row();
        if ($upload->type == 'file'){
            unlink('img' . '/' . $upload->name);
            $this->db->where('id', $id);
            $this->db->delete('uploads');
        }
        if ($upload->type == 'image'){
            unlink('img' . '/' . $upload->name);
            unlink('img' . '/' . $upload->thumb);
            $this->db->where('id', $id);
            $this->db->delete('uploads');
        }
    }

    public function get_menus(){
        // Get normalized menu array
        // $menu[menu number][item info] -> Top menu has number 0, sub menus have subsequent numbers, menu[1],menu[2] etc
        $this->db->from('menu');
        $menu_content = $this->db->get();
        $results = $menu_content->result_array();
        foreach($results as $result){
            if ($result['type'] == 'parent' || ($result['type'] == 'link' && $result['parent'] == 0)){
                $menu[0][] = $result; // Topmenu items go in menu[0], subsequent sub menus in menu[1],menu[2] etc
            } else {
                $menu[$result['parent']][] = $result;
            }
        }
        return $menu;
    }

}