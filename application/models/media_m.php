<?php
class Media_m extends MY_Model
{
    protected $_table = 'media';
    public $validate = array(
        'title' => array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required|max_length[100]|xss_clean|callback__unique_title'
        ),
        'year' => array(
            'field' => 'year',
            'label' => 'Year',
            'rules' => 'trim|max_length[100]|xss_clean'
        ),
        'author' => array(
            'field' => 'author',
            'label' => 'Author',
            'rules' => 'trim|max_length[100]|xss_clean'
        ),
        'text' => array (
            'field' => 'text',
            'label' => 'Text',
            'rules' => 'xss_clean'
        )
    );

    public function get_new ()
    {
        $media = new stdClass();
        $media->parent_id = '';
        $media->parent_table = '';
        $media->title = '';
        $media->type = '';
        $media->description = '';
        $media->uri = '';
        $media->published = '';
        return $media;
    }

    public function get_media(){
        $parent_table = $this->uri->segment(2);
        $parent_id = $this->uri->segment(4);
        if ($parent_table == 'obras') $parent_table = 'work';
        $this->db->from('media');
        $this->db->where('parent_id', $parent_id);
        $this->db->where('parent_table', $parent_table);
        $this->db->order_by('ordering', 'asc');
        $this->db->order_by('type', 'asc');
        $media = $this->db->get();
        return $media->result_array();
    }
    public function get_new_order($post){
        $this->db->from('media');
        $this->db->select('ordering');
        $this->db->where('parent_id', $post['parent_id']);
        $this->db->where('parent_table', $post['parent_table']);
        $this->db->order_by('ordering', 'desc');
        $result = $this->db->get();
        $highest_order = $result->row();
        return $highest_order->ordering + 10;
    }



    public function register_image ($post, $data){

        $order = $this->get_new_order($post);

        switch ( $post['type']){
            case 'img':
                $insert = array (
                    'parent_table' => $post['parent_table'],
                    'parent_id' => $post['parent_id'],
                    'uri' => $data['file_name'],
                    'title' => $post['title'],
                    'type' => $post['type'],
                    'published' => '1',
                    'description' => $post['description'],
                    'ordering' => $order
                );
                break;
            case 'pdf':
                $insert = array (
                    'parent_table' => $post['parent_table'],
                    'parent_id' => $post['parent_id'],
                    'uri' => $data['file_name'],
                    'title' => '1',
                    'type' => $post['type'],
                    'published' => '1',
                    'description' => '1'
                );
                break;
            case 'vim':
                $insert = array (
                    'parent_table' => $post['parent_table'],
                    'parent_id' => $post['parent_id'],
                    'uri' => $data['file_name'],
                    'title' => $post['title'],
                    'type' => $post['type'],
                    'published' => '1',
                    'description' => $post['description'],
                    'ordering' => $order
                );
                break;
        }



        $this->insert($insert);
    }



    public function delete_from_disk ($id) {
        $this->load->helper('file');

        //delete database entry and file on disk and evt. thumb
        $data = $this->get($id);

        //delete db entry
        $this->delete($id);

        //delete if file
        if ($data->type !== 'vim'){
            $thumb = substr($data->uri, 0, -4) . '_thumb.' . substr($data->uri, -3);
            unlink('media/' . $data->uri);
            unlink('media/' . $thumb);
        }
    }
}